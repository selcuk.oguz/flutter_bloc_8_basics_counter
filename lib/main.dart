import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_test_bl_app/bloc/counter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CounterBloc(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // int _counter = 0;

  void _incrementCounter() {
    BlocProvider.of<CounterBloc>(context).add(CounterIncrement());
  }

  void _decrementCounter() {
    BlocProvider.of<CounterBloc>(context).add(CounterDecrement());
  }

  void _changeToFive() {
    BlocProvider.of<CounterBloc>(context).add(CounterChange(value: 5));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'You have pushed the button this many times:',
                ),
                // BlocBuilder<CounterBloc, CounterInitial>(
                //   builder: (context, state) {
                //     return Text(
                //       "${state.count}",
                //       style: Theme.of(context).textTheme.headline4,
                //     );
                //   },
                // ),

                // BlocListener<CounterBloc, CounterInitial>(
                //   listener: (context, state) {
                //     ScaffoldMessenger.of(context).showSnackBar(
                //       SnackBar(
                //         content: Text("value is ${state.count}"),
                //       ),
                //     );
                //   },
                //   child: Text(
                //     "???",
                //     style: Theme.of(context).textTheme.headline4,
                //   ),
                // ),

                BlocConsumer<CounterBloc, CounterInitial>(
                  listener: (context, state) {
                    ScaffoldMessenger.of(context).clearSnackBars();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text("value is ${state.count}"),
                      ),
                    );
                  },
                  builder: (context, state) {
                    return Text(
                      "${state.count}",
                      style: Theme.of(context).textTheme.headline4,
                    );
                  },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FloatingActionButton(
                  onPressed: _incrementCounter,
                  tooltip: 'Increment',
                  child: const Icon(Icons.add),
                ),
                FloatingActionButton(
                  onPressed: _changeToFive,
                  tooltip: 'To Five',
                  child: const Icon(Icons.five_k_plus),
                ),
                FloatingActionButton(
                  onPressed: _decrementCounter,
                  tooltip: 'Decrement',
                  child: const Icon(Icons.remove),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

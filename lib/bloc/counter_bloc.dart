import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterInitial> {
  CounterBloc() : super(CounterInitial(count: 0)) {
    on<CounterIncrement>(
      (event, emit) => emit(
        CounterInitial(count: state.count + 1),
      ),
    );

    on<CounterDecrement>(
      (event, emit) => emit(
        CounterInitial(count: state.count - 1),
      ),
    );

    on<CounterChange>(
      (event, emit) => emit(
        CounterInitial(count: event.value),
      ),
    );
  }
}
